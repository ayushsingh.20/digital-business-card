import React from "react";
import ReactDOM from "react-dom/client"


function Info(){
  return (
    <div className="info">
        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Shri_Virat_Kohli_for_Cricket%2C_in_a_glittering_ceremony%2C_at_Rashtrapati_Bhavan%2C_in_New_Delhi_on_September_25%2C_2018_%28cropped%29.JPG/330px-Shri_Virat_Kohli_for_Cricket%2C_in_a_glittering_ceremony%2C_at_Rashtrapati_Bhavan%2C_in_New_Delhi_on_September_25%2C_2018_%28cropped%29.JPG" alt="" />
    
        <h1>Virat Kohli</h1>
        
        <h4>Cricketer &amp; Athelete</h4>
        
        <a href="https://en.wikipedia.org/wiki/Virat_Kohli" target="_blank">wikipedia.org</a>
    
        <div className="infoButton">
            <a href="https://viratkohli.foundation/contacts-us/" target="_blank" >
            <button className="white-btn"><i className="fa fa-envelope"></i>Email</button>
            </a>
            <a href="https://www.linkedin.com/in/janit-desai/?originalSubdomain=in" target="_blank">
            <button className="blue-btn"><i className="fa fa-linkedin-square"></i>LinkedIn</button>
            </a>
        </div>
    </div>
  )
}

function About(){
    return (
        <div className="section">
            <h1>About</h1>
            
            <p>
            Virat Kohli is an Indian international cricketer and former captain of the Indian national team. Widely regarded as one of the greatest batsmen of all time, Kohli plays as a right-handed batsman for Royal Challengers Bangalore in the IPL and for Delhi in Indian domestic cricket.
             </p>
        </div>
    )
}

function Interests(){
    return (
        <div className="section">
            <h1>Interests</h1>
            
            <p>
            Virat Kohli is a fitness freak. When not on duty Kohli also sneaks out time to travel as well. Being a Punjabi lad, Virat Kohli enjoys listening to Punjabi upbeat songs as well as dancing to a few groovy numbers. 
             </p>
        </div>
    )    
}

function Footer(){
    return (
        <div className="footer">
            <a href="https://twitter.com/imVkohli" target="_blank"><i className="fa fa-twitter-square fa-2x"></i></a>
            <a href="https://www.facebook.com/virat.kohli/" target="_blank"><i className="fa fa-facebook-official fa-2x"></i></a>
            <a href="https://www.instagram.com/virat.kohli/" target="_blank"><i className="fa fa-instagram fa-2x"></i></a>
            <a href="https://github.com/topics/virat-kohli" target="_blank"><i className="fa fa-github-square fa-2x"></i></a>
        </div>
    )    
}


function App() {
    return (
    <div className="container">
     <Info />
     <About />
     <Interests />
     <Footer />
    </div>
    )
}



const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(<App />);